import http from 'k6/http';
import { Trend } from 'k6/metrics';
import { check } from 'k6';
import { group } from 'k6';

export let options = {
    thresholds: {
      http_req_failed: ['rate<0.01'],   // http errors should be less than 1% 
      http_req_duration: ['p(95)<200'], // 95% of requests should be below 200ms
    },
    scenarios: {
        my_awesome_api_test: {
          executor: 'constant-vus',
          vus: 10,
          duration: '2m',
        },
    }
}
export default function () {
    group('visit product listing page', function () {
        let response = http.get("https://test-api.k6.io/");
        check(response, {
            'is status 200': (r) => r.status === 200,
          });        
      });
    
      group('Test page website on error', function () {
        let response = http.get("https://test-api.k6.io/public/crocodiles/ddddd");
        check(response, {
            'is status 200': (r) => r.status === 200,
          });        
      });    
}